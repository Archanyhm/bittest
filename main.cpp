#include <iostream>
#include <cmath>
#include <limits>
#include <GraphicsMagick/Magick++.h>

void getBits(unsigned char byte) {
  int amountOfBits = sizeof(unsigned char) * 8;

  for (int i = amountOfBits-1; i>=0; --i)
  {
    std::cout << ((byte >> i) & (uintmax_t)1);
  }
}

void getBits(int number) {
  int amountOfBits = sizeof(int) * 8;

  for(int i = amountOfBits-2; i>=0; --i)
  {
    std::cout << ((number >> i) & (uintmax_t)1);
  }
}

unsigned char setLSB(unsigned char byte) {
  return byte |= (uintmax_t)1;
}

void xorLSB(unsigned char byte[8], unsigned char payload) {
  unsigned char shifted_values[8];

  int counter = 7;
  for(int i = 0; i<8; ++i)
  {
    shifted_values[i] = (byte[i] & ~((uintmax_t)1 << 0)) | ((payload >> counter--) << 0);
  }
}

uint8_t replace_bits(uint8_t byte, uint8_t payload, unsigned short n, unsigned short mask_shift = 0, unsigned short payload_shift = 0) {
  constexpr uint8_t mask[8] = {0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F, 0xFF};

  return (n<8) ? ((byte & ~(mask[n-1] << mask_shift)) | ((payload >> payload_shift) & mask[n-1])) : 0;
}

uint16_t replace_bits_short (uint16_t bytes,
                             uint8_t payload,
                             unsigned short n,
                             unsigned short mask_shift = 0,
                             unsigned short payload_shift = 0) {
  constexpr uint16_t mask[16] = {0x0001, 0x0003, 0x0007, 0x000F,
                                 0x001F, 0x003F, 0x007F, 0x00FF,
                                 0x01FF, 0x03FF, 0x07FF, 0x0FFFF,
                                 0x1FFF, 0x3FFF, 0x7FFF, 0xFFFF};

  return (n<16) ? ((bytes & ~(mask[n-1] << mask_shift)) | ((payload >> payload_shift) & mask[n-1])) : 0;
}

void printXorLSB(uint8_t byte[8], uint8_t payload) {
  unsigned char shifted_values[8];

  int counter = 7;
  for(int i = 0; i<8; ++i)
  {
    if(counter<0)
      break;

    shifted_values[i] = replace_bits(byte[i], payload, 1, 0, counter--);
    //shifted_values[i] = (byte[i] & ~((uintmax_t)1 << 0)) | ((payload >> counter--) & (uintmax_t)1 << 0);
  }


  std::cout << "Bits         Mod : Ori" << "\n"
            << "----------------------" << std::endl;
  for(int i = 0; i<8; ++i)
  {
    for(size_t j = sizeof(unsigned char)*8; j>0; --j)
    {
      if(j>1) {
        std::cout << ((shifted_values[i] >> (j-1)) & (uintmax_t)1);
      } else {
        std::cout << " ";
        for(int k = 1; k>0; --k)
        {
          std::cout << ((shifted_values[i] >> (j-1)) & (uintmax_t)1);
        }
        std::cout << " == " << static_cast<int>(shifted_values[i]) << " : " << static_cast<int>(byte[i]);
        break;
      }
    }
   std::cout << std::endl;
  }
  std::cout << "----------------------" << "\n"
            << "Payload: " << static_cast<int>(payload) << "|";
  getBits(payload);
  std::cout << "\n" << std::endl;
}

void create_empty_square() {
  Magick::Image square(Magick::Geometry(100, 100), "white");
  square.quiet(false);
  square.modulusDepth(16);
  square.modifyImage();
  square.write("square.png");
}

void insert_payload(Magick::Image &image, std::string payload)
{
  int w = image.columns();
  int h = image.rows();

  int max_bytes = image.depth() == 16 ? w * h * 3 : w * h * 3 / 8;

  if(sizeof(payload) > max_bytes) {
    std::cout << "Data doesn't fit into image" << std::endl;
    return;
  } else
  {
    std::cout << max_bytes << std::endl;
  }

  Magick::Pixels view(image);
  Magick::PixelPacket *pixels = view.get(0,0,w,h);

  for(size_t i = 0; i<payload.size(); i+=3)
  {
    Magick::Quantum store[3] = {pixels->red, pixels->green, pixels->blue};
    pixels->red = replace_bits_short(store[0], payload[i], 8, 0, 0);
    pixels->green = replace_bits_short(store[1], payload[i+1], 8, 0, 0);
    pixels++->blue = replace_bits_short(store[2], payload[i+2], 8, 0, 0);
    view.sync();
  }
  view.sync();
  image.write("modifys.png");
}

void read_payload(Magick::Image &image)
{
  std::cout << "Start..." << std::endl;

  int w = image.columns();
  int h = image.rows();

  Magick::Pixels view(image);
  Magick::PixelPacket *pixels = view.get(0,0,w,h);

  int null_byte_counter = 0;

  for(int rows = 0; rows < w; ++rows)
  {
    if(null_byte_counter > 2)
      break;

    for(int columns = 0; columns < h; ++columns)
    {
      if(null_byte_counter > 2)
        break;

      Magick::Quantum store[3] = {pixels->red, pixels->green, pixels->blue};
      ++pixels;
      unsigned char casted_values[3] = {static_cast<unsigned char>(store[0]), static_cast<unsigned char>(store[1]), static_cast<unsigned char>(store[2])};

      for(int it = 0; it<3; ++it)
      {
        if(casted_values[it] == 0xFF && null_byte_counter < 2)
          ++null_byte_counter;
        else if(casted_values[it] != 0xFF && null_byte_counter < 2) {
          std::cout << casted_values[it];
          null_byte_counter = 0;
        } else if(null_byte_counter > 2)
          break;
      }
    }
  }
}

int main(int argc, char *argv[])
{
  Magick::InitializeMagick(*argv);
  create_empty_square();

  Magick::Image image;
  image.quiet(false);
  image.read("square.png");
  image.modifyImage();

  if(image.depth() != 16)
    return 0;

//  int w = image.columns();
//  int h = image.rows();

//  Magick::Pixels view(image);
//  Magick::PixelPacket *pixels = view.get(0,0,w,h);

  insert_payload(image, "This could be some arbitrary payload. Yay!");

  image.read("modifys.png");
  read_payload(image);




//  for(int row = 0; row < w; ++row) {
//    for(int column = 0; column < h; ++column)
//    {
//std::cout << pixels++->red << "\n";
//    }
//  }
//  view.sync();

//  image.write("modifys.png");


  uint8_t bytes[8] = {128, 101, 143, 185, 127, 127, 131, 255};
  //getBits(test);
  //printXorLSB(bytes, 0b10101010);
  //printXorLSB(bytes, 1);

  //std::cout << static_cast<int>(replace_bits(128, 0xFF, 1, 0, 7)) << std::endl;


  return 0;
}
